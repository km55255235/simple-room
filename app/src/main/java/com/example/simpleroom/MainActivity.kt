package com.example.simpleroom

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val dataSource = SimpleDatabase.getInstance(this).simpleChartDao

        //insert
        val btnAction = findViewById<Button>(R.id.btn_action)
        btnAction.setOnClickListener {
            dataSource.insert(SimpleChart(itemName = "Ayam Bakar", itemQuantity = 1))
        }
        dataSource.getAllItem().observe(this){
            Log.e("SimpleDataBase", it.toString())
        }
    }
}